﻿using System;
using System.Collections;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    [SerializeField] private Transform _explodeFX;
    [SerializeField] private Projectile _projectile;
    private float _pushSpeed = 0.125f;
    [SerializeField] private float _elapsedTime = 99;
    [SerializeField] private bool _isMoving = false;
    private Vector3 _direction = Vector3.zero;
    private Rigidbody2D _rb;
    private Tuple<Vector2, Vector2>[] _rays;
    [SerializeField] private Color _color;

    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _rays = new Tuple<Vector2, Vector2>[4];
        _color = GetComponent<SpriteRenderer>().color;
    }

    private void Update()
    {
        _elapsedTime += Time.deltaTime;
        if (!_isMoving)
            return;

        if (_elapsedTime <= _pushSpeed)
            return;

        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position + _direction, 0.1f);
        foreach (var c in colliders)
        {
            if (c.CompareTag("Bomb"))
            {
                _isMoving = false;
                Bomb b = c.GetComponent<Bomb>();
                Vector3 dir = Vector3.Normalize(b.transform.position - transform.position);
                b.Push(dir);
                return;
            }
            else
            {
                _isMoving = false;
                return;
            }
        }

        transform.position += _direction;
        transform.position = new Vector3(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y), Mathf.RoundToInt(transform.position.z));
        _elapsedTime = 0;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        //_rb.velocity = Vector2.zero;
    }

    private void OnDrawGizmos()
    {
        if (_rays == null)
            return;

        foreach (Tuple<Vector2, Vector2> tuple in _rays)
        {
            if (tuple == null) continue;
            Gizmos.DrawLine(tuple.Item1, tuple.Item2);
            Gizmos.DrawSphere(tuple.Item2, 0.1f);
        }
    }

    public bool Push(Vector3 direction)
    {
        _direction = direction;

        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position + _direction, 0.1f);
        if (colliders.Length > 0) return false;

        _isMoving = true;
        //transform.position += _direction;
        //_rb.velocity = _direction * _pushSpeed;
        return true;
    }

    private void FixedUpdate()
    {
    }

    public void Explode(float delay = 0.5f)
    {
        // Cast a ray in four direction
        //_rays[0] = RaycastCheck(transform.position + new Vector3(0, 0.6f), Vector2.up);
        //_rays[1] = RaycastCheck(transform.position + new Vector3(0, -0.6f), Vector2.down);
        //_rays[2] = RaycastCheck(transform.position + new Vector3(0.6f, 0), Vector2.right);
        //_rays[3] = RaycastCheck(transform.position + new Vector3(-0.6f, 0), Vector2.left);
        StartCoroutine(IEExplode(delay));
    }

    private IEnumerator IEExplode(float delay = 0)
    {
        yield return new WaitForSeconds(delay);

        // Shoot projectiles in all directions
        Projectile p1 = Instantiate(_projectile, transform.position + new Vector3(0, 0.6f), Quaternion.Euler(0, 0, 90));
        Projectile p2 = Instantiate(_projectile, transform.position + new Vector3(0, -0.6f), Quaternion.Euler(0, 0, -90));
        Projectile p3 = Instantiate(_projectile, transform.position + new Vector3(0.6f, 0), Quaternion.Euler(0, 0, 0));
        Projectile p4 = Instantiate(_projectile, transform.position + new Vector3(-0.6f, 0), Quaternion.Euler(0, 0, 180));

        Transform t = Instantiate(_explodeFX, transform.position, Quaternion.identity);
        Destroy(t.gameObject, 1);
        Destroy(gameObject);

        yield return null;
    }

    private Tuple<Vector2, Vector2> RaycastCheck(Vector2 origin, Vector2 direction)
    {
        Vector2 start = origin;
        Vector2 end = Vector2.zero;
        RaycastHit2D hit = Physics2D.Raycast(start, direction);

        // If it hits something...
        if (hit.collider != null)
        {
            end = hit.point;
            float distance = Mathf.Abs(end.y - transform.position.y);

            Debug.LogFormat("<color=lime>{0} | {1}</color>", hit.collider.name, distance);
        }

        return new Tuple<Vector2, Vector2>(start, end);
    }
}