﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canon : MonoBehaviour
{
    [SerializeField] private Projectile _projectile;
    [SerializeField] private Transform _projectilePosition;
    [SerializeField] private float _fireSpeed = 1;
    private float _elapsedTime = 0;
    private Button _button;

    void Start()
    {
        _button = FindObjectOfType<Button>();
        if (_button != null)
        {
            _button.onButtonDown += () => Fire();
        }
    }

    void Fire()
    {
        Instantiate(_projectile, _projectilePosition.position, transform.rotation);
    }
}
