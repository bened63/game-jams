﻿using System;
using UnityEngine;

public class Button : MonoBehaviour
{
    [SerializeField] private GameObject _outline;
    private bool _buttonActive = false;
    private bool _canPressed = true;
    private float _elapsedTime = 0;

    public Action onButtonDown;

    private void Update()
    {
        _elapsedTime += Time.deltaTime;
        if (_elapsedTime < 1)
        {
            _canPressed = false;
            return;
        }

        _canPressed = true;

        if (_canPressed && Input.GetKeyDown(KeyCode.E))
        {
            _elapsedTime = 0;
            onButtonDown?.Invoke();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            onButtonDown?.Invoke();
            //_outline.SetActive(true);
            //_buttonActive = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            //_outline.SetActive(false);
            //_buttonActive = false;
        }
    }
}