﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private float _speed;
    private SpriteRenderer _spriteRenderer;
    private Rigidbody2D _rb;
    private Vector2 _velocity = Vector2.zero;
    private Color _color;

    public Color color
    {
        get => _color;
        set
        {
            _color = value;
            _spriteRenderer.color = value;
        }
    }

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _velocity = transform.right * _speed;
        _rb.velocity = _velocity;
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Bomb"))
        {
            Bomb b = collision.gameObject.GetComponent<Bomb>();
            b.Explode();
        }
        Destroy(gameObject);
    }
}