﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    [SerializeField] private float _runSpeed = 2;
    [SerializeField] private ParticleLauncher _particleLauncher;
    private Camera _mainCam;
    private Rigidbody2D _rb;
    private AudioSource _audioSource;
    private float _horizontal;
    private float _vertical;
    private Animator _anim;
    private UIManager _ui;
    private int _currentLevel = 1;

    // Start is called before the first frame update
    private void Start()
    {
        _mainCam = Camera.main;
        _rb = GetComponent<Rigidbody2D>();
        _audioSource = GetComponent<AudioSource>();
        _anim = GetComponent<Animator>();
        _ui = FindObjectOfType<UIManager>();
        if (_ui == null) Debug.Log("<color=lime>No UIManager found.</color>");
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
            SceneManager.LoadScene(_currentLevel, LoadSceneMode.Additive);
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            _currentLevel = 1;
            SceneManager.LoadScene(0);
            //SceneManager.UnloadSceneAsync(_currentLevel);
            SceneManager.LoadScene(1, LoadSceneMode.Additive);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            _currentLevel = 2;
            SceneManager.LoadScene(0);
            //SceneManager.UnloadSceneAsync(_currentLevel);
            SceneManager.LoadScene(2, LoadSceneMode.Additive);
        }

        _horizontal = Input.GetAxisRaw("Horizontal");
        _vertical = Input.GetAxisRaw("Vertical");

        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D) ||
            Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S))
        {
            Move();
        }

        //if (_horizontal != 0) _anim.SetBool("IsRunning", true);
        //else _anim.SetBool("IsRunning", false);

        //LookAtMouse();
        Fire();
    }

    private void Move()
    {
        Vector3 direction = new Vector3(_horizontal, _vertical, 0);

        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position + direction, 0.1f);
        foreach (var c in colliders)
        {
            if (c.CompareTag("Bomb"))
            {
                Bomb b = c.GetComponent<Bomb>();
                Vector3 dir = Vector3.Normalize(b.transform.position - transform.position);
                if (!b.Push(dir)) return;
            }
            else if (!c.CompareTag("Button"))
            {
                return;
            }
        }
        transform.Translate(direction);
    }

    private void FixedUpdate()
    {
        //Vector3 velocity = new Vector3(_horizontal, _vertical) * Time.fixedDeltaTime;
        //velocity = velocity.normalized * _runSpeed;
        //_rb.velocity = velocity;
    }

    private void LookAtMouse()
    {
        Vector3 dir = Input.mousePosition - _mainCam.WorldToScreenPoint(transform.position);
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    private void Fire()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            //_particleLauncher.Emit();
        }

        //_lastFired += Time.deltaTime;
        //if (_lastFired >= _fireCooldown && Input.GetButtonDown("Jump"))
        //{
        //    _lastFired = 0;
        //    _audioSource.clip = _shootClip;
        //    _audioSource.Play();
        //    _anim.SetTrigger("Shoot");
        //    Instantiate(_spell, _hand.position, Quaternion.Euler(0, 0, _hand.eulerAngles.y));
        //}
    }

    public void Hit()
    {
        //if (_isDead)
        //    return;

        //Instantiate(_deathEffect, transform.position, Quaternion.identity);
        //Instantiate(_deathSprite, transform.position, Quaternion.identity);
        //_isDead = true;

        //_audioSource.clip = _dieClip;
        //_audioSource.Play();

        //// Remove Player from screen
        //GetComponent<SpriteRenderer>().enabled = false;
    }
}