﻿using System.Collections.Generic;
using UnityEngine;

public class ParticleLauncher : MonoBehaviour
{
    private ParticleSystem _particleLauncher;
    private ParticleSystem.MainModule _mainModule;
    private Color _color;

    private List<ParticleCollisionEvent> _collisionEvents;

    private void Start()
    {
        _particleLauncher = GetComponent<ParticleSystem>();
        _collisionEvents = new List<ParticleCollisionEvent>();
        _mainModule = _particleLauncher.main;
        _color = _mainModule.startColor.color;
    }

    private void OnParticleCollision(GameObject other)
    {
        //ParticlePhysicsExtensions.GetCollisionEvents(_particleLauncher, other, _collisionEvents);
        if (other.CompareTag("Bomb"))
        {
            Bomb b = other.GetComponent<Bomb>();
            b.Explode();
        }
    }

    public void Emit()
    {
        _particleLauncher.Emit(1);
    }
}