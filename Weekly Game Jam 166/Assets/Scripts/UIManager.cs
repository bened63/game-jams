﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private Text _treeLive;
    [SerializeField] private GameObject _winPanel;
    [SerializeField] private GameObject _lostPanel;

    public void UpdateLives(int lives)
    {
        _treeLive.text = lives.ToString();
    }

    public void ShowWinMessage()
    {
        _winPanel.SetActive(true);
    }

    public void ShowLostMessage()
    {
        _lostPanel.SetActive(true);
    }
}
