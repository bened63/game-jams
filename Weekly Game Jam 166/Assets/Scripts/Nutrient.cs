﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nutrient : MonoBehaviour
{
    private Tree _tree;

    private void Start()
    {
        _tree = FindObjectOfType<Tree>();
        if (_tree == null)
        {
            Debug.Log("<color=lime>Could not find object of type Tree.</color>");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        _tree.Grow();

        if (collision.CompareTag("Player"))
        {
            Player p = collision.GetComponent<Player>();
            p.AddNutrient();
        }
        Destroy(gameObject);
    }
}
