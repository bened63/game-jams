﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] Transform _nutrient;
    [SerializeField] private GameObject _container;
    [SerializeField] private int _count = 10;
    [SerializeField] Vector3 _topLeft;
    [SerializeField] Vector3 _topRight;
    [SerializeField] Vector3 _bottomLeft;
    [SerializeField] Vector3 _bottomRight;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < _count; i++)
        {
            float x = Random.Range(_topLeft.x, _topRight.x);
            float y = Random.Range(_topLeft.y, _bottomLeft.y);

            Vector3 pos = new Vector3(Mathf.RoundToInt(x) * 0.25f, Mathf.RoundToInt(y) * 0.25f, 0);

            // Check if another nutirent is near
            if (Physics2D.OverlapCircle(pos, 1))
            {
                continue;
            }

            Transform go = Instantiate(_nutrient, pos, Quaternion.identity);
            go.parent = _container.transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(_topLeft, _topRight);
        Gizmos.DrawLine(_topRight, _bottomRight);
        Gizmos.DrawLine(_bottomRight, _bottomLeft);
        Gizmos.DrawLine(_bottomLeft, _topLeft);
    }
}
