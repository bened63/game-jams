﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour
{
    [SerializeField] private int _lives = 100;
    [SerializeField] private float _growStrength = 0.5f;
    private UIManager _uiManager;
    private Player _player;

    private void Start()
    {
        _uiManager = FindObjectOfType<UIManager>();
        if (_uiManager == null)
        {
            Debug.Log("<color=lime>Could not find UIManager.</color>");
        }

        _player = FindObjectOfType<Player>();
        if (_player == null)
        {
            Debug.Log("<color=lime>Could not find Player.</color>");
        }
    }

    public void ReduceLife()
    {
        _lives--;
        _uiManager.UpdateLives(_lives);
        if (_lives <= 0)
        {
            _player.allowUserInput = false;
            _uiManager.ShowLostMessage();
        }
    }

    public void Grow()
    {
        _lives += 10;
        _uiManager.UpdateLives(_lives);
        Vector3 scale = transform.localScale + new Vector3(_growStrength, _growStrength, 0);
        transform.localScale = scale;
    }
}
