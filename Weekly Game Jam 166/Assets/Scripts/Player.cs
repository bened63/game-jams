﻿using UnityEngine;

public class Player : MonoBehaviour
{
    private float _speed = 0.25f;
    private LineRenderer _lineRenderer;
    private Tree _tree;
    private Vector3 _gizmoPos = Vector3.zero;
    [SerializeField] private int _collectedNutrients = 0;
    private UIManager _uiManager;

    public bool allowUserInput { get; set; } = true;

    private void Start()
    {
        _lineRenderer = GetComponent<LineRenderer>();
        _lineRenderer.SetPosition(0, transform.position);

        _tree = FindObjectOfType<Tree>();
        if (_tree == null)
        {
            Debug.Log("<color=lime>Could not find object of type Tree.</color>");
        }

        _uiManager = FindObjectOfType<UIManager>();
        if (_uiManager == null)
        {
            Debug.Log("<color=lime>Could not find UIManager.</color>");
        }
    }

    private void Update()
    {
        if (!allowUserInput)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            UpdatePosition(Vector3.up);
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            UpdatePosition(Vector3.down);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            UpdatePosition(Vector3.left);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            UpdatePosition(Vector3.right);
        }
    }

    private void UpdatePosition(Vector3 pos)
    {
        Vector3 newPos = transform.position + pos * _speed;
        _gizmoPos = newPos;

        // Check if above ground
        if (newPos.y >= 3)
        {
            return;
        }

        // Check if there is no blocker
        Collider2D c = Physics2D.OverlapCircle(newPos, 0.05f);
        if (c != null)
        {
            if (c.tag == "Obstacle") return;
        }

        // Move
        transform.position = newPos;

        // Check if point already visited
        bool visited = false;
        for (int i = 0; i < _lineRenderer.positionCount; i++)
        {
            Vector3 p = _lineRenderer.GetPosition(i);
            if (p == transform.position)
            {
                visited = true;
                break;
            }
        }
        if (!visited)
        {
            _tree.ReduceLife();
        }

        // Add point to line
        _lineRenderer.positionCount++;
        _lineRenderer.SetPosition(_lineRenderer.positionCount - 1, transform.position);
    }

    public void AddNutrient()
    {
        _collectedNutrients++;
        if (_collectedNutrients >= 12)
        {
            allowUserInput = false;
            _uiManager.ShowWinMessage();
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(_gizmoPos, 0.15f);
    }
}