﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject _win;

    public void ShowWinMessage()
    {
        _win.SetActive(true);
    }
}
