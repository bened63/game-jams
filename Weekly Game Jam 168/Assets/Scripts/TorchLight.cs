﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchLight : MonoBehaviour
{
    [SerializeField] private float _flickerStrength = 1;
    [SerializeField] private float _speed = 1;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(IEFlickering());
    }

    IEnumerator IEFlickering()
    {
        while (true)
        {
            float x = Random.Range(-_flickerStrength, _flickerStrength);
            float y = Random.Range(-_flickerStrength, _flickerStrength);
            Vector2 flicker = new Vector3(x, y);
            transform.localPosition = flicker;
            yield return new WaitForSeconds(_speed);
        }
    }
}
