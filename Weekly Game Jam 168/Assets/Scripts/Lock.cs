﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lock : MonoBehaviour
{
    public float _openingSpeed = 0.5f;
    bool _isOpen = false;

    public void Open()
    {
        if (_isOpen)
            return;

        _isOpen = true;
        StartCoroutine(IEOpen());
    }

    IEnumerator IEOpen()
    {
        bool _running = true;
        while (_running)
        {
            if (transform.position.y < -2f) _running = false;
            transform.Translate(Vector3.down * Time.deltaTime * _openingSpeed);
            yield return null;
        }

    }
}
