﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPosition : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Torch"))
        {
            Torch t = collision.GetComponent<Torch>();
            t.Swap(transform);
        }
    }

    public void Follow()
    {

    }
}
