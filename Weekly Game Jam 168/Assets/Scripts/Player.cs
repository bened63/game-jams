﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    [SerializeField] private float _runSpeed = 30;
    [SerializeField] private Transform _hand;
    [SerializeField] private Torch _torch;
    private bool _hasTorch = true;
    private SimpleCharacterController2D _controller;
    private SimpleCharacterController2D _followerController;
    private float _horizontal;
    private bool _jump = false;
    private bool _crouch = false;
    private Animator _anim;
    private Transform _follower;
    private bool _hasFollower = false;

    // Start is called before the first frame update
    private void Start()
    {
        _controller = GetComponent<SimpleCharacterController2D>();
        _anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            // Torch back to active player
            TakeTorch();
        }

        if (!_hasTorch)
        {
            _horizontal = 0;
            return;
        }

        _horizontal = Input.GetAxisRaw("Horizontal") * _runSpeed;
        if (_horizontal != 0)
        {
            _anim.SetBool("IsWalking", true);
        }
        else
        {
            _anim.SetBool("IsWalking", false);
        }

        if (Input.GetButtonDown("Jump"))
        {
            _anim.SetTrigger("IsJumping");
            _jump = true;
        }
        else if (Input.GetButtonUp("Fire1"))
        {
            ThrowTorch();
        }
    }

    private void FixedUpdate()
    {
        _controller.Move(_horizontal * Time.fixedDeltaTime, _crouch, _jump);
        if (_hasFollower)
        {
            _follower.position = transform.position - new Vector3(0.599f, 0, 0);
        }
        _jump = false;
    }

    private void ThrowTorch()
    {
        if (!_hasTorch)
            return;

        _torch.Throw(new Vector3(0, 1, 0) +  transform.right);
        _hasTorch = false;
    }

    public void TakeTorch()
    {
        _hasTorch = true;
        _torch.gameObject.GetComponent<Rigidbody2D>().simulated = false;
        _torch.transform.parent = _hand.transform;
        _torch.transform.localPosition = Vector2.zero;
        _torch.transform.localRotation = Quaternion.identity;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerPosition"))
        {
            PlayerPosition pp = collision.GetComponent<PlayerPosition>();
            pp.Follow();
            _follower = pp.transform;
            _hasFollower = true;
        }
    }
}