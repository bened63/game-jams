﻿using System.Collections;
using UnityEngine;

public class Torch : MonoBehaviour
{
    [SerializeField] float _thrust = 2;
    private Rigidbody2D _rb;
    private CircleCollider2D _collider;
    [SerializeField] private Player _player;

    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _collider = GetComponent<CircleCollider2D>();
    }

    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.CompareTag("Player"))
    //    {
    //        _rb.simulated = false;
    //        if (_player == null) _player = collision.GetComponent<Player>();
    //        _player.TakeTorch(GetComponent<Torch>());
    //    }
    //}

    public void Throw(Vector2 force)
    {
        StartCoroutine(IEThrow(force));
    }

    private IEnumerator IEThrow(Vector2 force)
    {
        _collider.enabled = false;
        _rb.simulated = true;
        _rb.velocity = Vector3.zero;
        _rb.AddForce(force * _thrust, ForceMode2D.Impulse);
        yield return new WaitForSeconds(0.2f);
        _collider.enabled = true;
    }

    public void Swap(Transform newPos)
    {
        Vector3 target = newPos.position;
        Vector3 oldPlayerPos = _player.transform.position;
        _player.transform.position = target;
        newPos.position = oldPlayerPos;

        _player.TakeTorch();
    }
}