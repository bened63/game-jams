﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndScreen : MonoBehaviour
{
    [SerializeField] private Text _collectedFood;
    [SerializeField] private Button _restart;
    [SerializeField] private PlayerModel _playerModel;

    private void Start()
    {
        _collectedFood.text = string.Format("You collected {0} food!", _playerModel.food);
        _restart.onClick.AddListener(RestartGame);
    }

    private void RestartGame()
    {
        SceneManager.LoadScene(1);
    }
}