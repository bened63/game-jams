﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private PlayerModel _playerModel;
    [SerializeField] private TimeOfDay _timeOfDay;
    [SerializeField] private Text _health;
    [SerializeField] private Text _stamina;
    [SerializeField] private Text _food;
    [SerializeField] private Text _time;
    [SerializeField] private Text _message;
    [SerializeField] private GameObject _gameOver;
    [SerializeField] private Button _restartButton;
    private bool _messageShown = false;

    [Space(20)] // 20 pixels of spacing here.
    [SerializeField] private GameObject _start;

    //[SerializeField] private GameObject _gameOver;
    //[SerializeField] private GameObject _win;

    private void Start()
    {
        UpdateHealth(_playerModel.health);
        UpdateStamina(_playerModel.stamina);
        UpdateFood(_playerModel.food);
        UpdateTime(6, 0);
        _playerModel.onHealthChanged += UpdateHealth;
        _playerModel.onStaminaChanged += UpdateStamina;
        _playerModel.onFoodChanged += UpdateFood;
        _playerModel.onTemperatureChanged += HandleTemperatureChanged;

        _timeOfDay.onTimeChanged += (h, m) => UpdateTime(h, m);

        _restartButton.onClick.AddListener(RestartGame);
    }

    private void RestartGame()
    {
        SceneManager.LoadScene(1);
    }

    private void HandleTemperatureChanged(PlayerModel.Temperature t)
    {
        if (t == PlayerModel.Temperature.Hot) ShowMessage("It's getting hot. Movement costs now 2.");
        if (t == PlayerModel.Temperature.Normal) ShowMessage("It's getting cooler. Movement costs 1 again.");
    }

    private void OnDestroy()
    {
        _playerModel.onHealthChanged -= UpdateHealth;
        _playerModel.onStaminaChanged -= UpdateStamina;
        _playerModel.onFoodChanged -= UpdateFood;
        _playerModel.onTemperatureChanged -= HandleTemperatureChanged;
    }



    private void ShowMessage(string msg)
    {
        //if (_messageShown)
        //    return;

        StartCoroutine(IEShow(_message.transform.parent.gameObject, 4));
        _message.text = msg;
    }

    private void UpdateTime(int hour, int minute)
    {
        _time.text = string.Format("Time: {0}:{1}", hour.ToString("00"), minute.ToString("00"));
    }

    private void UpdateHealth(float f)
    {
        _health.text = "Health: " + f.ToString("000");
    }

    private void UpdateStamina(float f)
    {
        _stamina.text = "Stamina: " + f.ToString("000");
    }

    private void UpdateFood(int i)
    {
        _food.text = "Food: " + i.ToString();
    }

    public void ShowGameOverMessage()
    {
        _gameOver.SetActive(true);
    }

    public void ShowWinMessage()
    {
        //_win.SetActive(true);
    }

    private IEnumerator IEHideShowMessage(float delay)
    {
        yield return new WaitForSeconds(delay);
        _start.SetActive(false);
        yield return null;
    }

    private IEnumerator IEShow(GameObject go, float duration)
    {
        _messageShown = true;
        go.SetActive(true);
        yield return new WaitForSeconds(duration);
        go.SetActive(false);
    }
}