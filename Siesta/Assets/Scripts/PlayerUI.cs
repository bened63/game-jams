﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    [SerializeField] private PlayerModel _playerModel;
    [SerializeField] private Text _addFoodText;

    private void Start()
    {
        _playerModel.onFoodChanged += ShowAddFoodText;
    }

    private void OnDestroy()
    {
        _playerModel.onFoodChanged -= ShowAddFoodText;
    }

    private void ShowAddFoodText(int i)
    {
        StartCoroutine(IEAnimateText(_addFoodText.transform, 1f));
    }

    private IEnumerator IEAnimateText(Transform text, float duration)
    {
        float lerpTime = 1f;
        float currentLerpTime = 0f;
        bool run = true;

        float moveDistance = 1f;

        Vector3 startPos = transform.position;
        Vector3 endPos = startPos + transform.up * moveDistance;

        // Show text
        text.gameObject.SetActive(true);

        // Start lerp loop
        while (run)
        {
            //increment timer once per frame
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime > lerpTime)
            {
                currentLerpTime = lerpTime;
            }

            // Lerp!
            float perc = currentLerpTime / lerpTime;
            text.position = Vector3.Lerp(startPos, endPos, perc);

            // End condition
            if (perc >= 1)
            {
                text.gameObject.SetActive(false);
                run = false;
            }

            yield return null;
        }
    }
}