﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TimeOfDay : MonoBehaviour
{
    [SerializeField] private PlayerModel _playerModel;
    [SerializeField, Min(0.15f)] private float _wait = 1;
    [SerializeField, Range(1, 60)] private int _addMinutesOnMove = 10;
    [SerializeField, Range(17, 24)] private int _finalHour = 20;
    private int _hour = 6;
    private int _minute = 0;

    public Action<int, int> onTimeChanged;

    private void Start()
    {
        //_playerModel.onPositionChanged += (i) => AddTime(_addMinutesOnMove);
        //StartCoroutine(IETimer());
    }

    private void AddTime(int addMinutes)
    {
        _minute = _minute + addMinutes;
        if (_minute >= 60)
        {
            _minute = 0;
            _hour++;
        }

        if (_hour >= _finalHour)
        {
            SceneManager.LoadScene(2);
        }
        onTimeChanged?.Invoke(_hour, _minute);
    }

    private IEnumerator IETimer()
    {
        while (true)
        {
            yield return new WaitForSeconds(_wait);
            AddTime(10);
        }
    }
}