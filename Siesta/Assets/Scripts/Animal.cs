﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class Animal : MonoBehaviour
{
    public enum Species
    {
        Normal, Static, AvoidPlayer, Fast, Big, Small
    }

    [SerializeField] private PlayerModel _playerModel;
    [SerializeField] private Species _species;
    [SerializeField] private GameObject _nextPositionMarker;
    private Tuple<Vector3, string>[] _neighbors = new Tuple<Vector3, string>[8];
    private bool _playerIsNear = false;
    private Vector3 _playerPosition;
    private Vector3 _nextDirection = Vector3.zero;
    private Vector3 _moveDirection = Vector3.zero;
    private LineRenderer _lineRenderer;
    //
    // N(-1, 1) N(0, 1) N(1,1)
    // N(-1, 0)    X    N(1,0)
    // N(-1 -1) N(0,-1) N(1,-1)
    //

    private void Start()
    {
        //UpdateNextDirection();
        _playerModel.onPositionChanged += HandleOnPositionChanged;
        _lineRenderer = GetComponent<LineRenderer>();
        _lineRenderer.material = new Material(Shader.Find("Sprites/Default"));
    }

    private void OnDestroy()
    {
        _playerModel.onPositionChanged -= HandleOnPositionChanged;
    }

    private void HandleOnPositionChanged(int i)
    {
        MoveToDirection(_moveDirection);
        UpdateNextDirection();
        _moveDirection = _nextDirection;
    }

    private void UpdateNextDirection()
    {
        if (_species == Species.Static)
            return;

        CheckNeighbors();

        // Default for next direction (no movement)
        _nextDirection = Vector3.zero;

        switch (_species)
        {
            case Species.Normal:

                for (int i = 0; i < 10; i++)
                {
                    Vector3 dir = GetRandomDirection();
                    if (GetTagAtPosition(transform.position + dir) == "Untagged")
                    {
                        _nextDirection = dir;
                        _nextPositionMarker.transform.position = transform.position + dir;
                        break;
                    }
                }
                break;

            case Species.AvoidPlayer:

                for (int i = 0; i < 10; i++)
                {
                    Vector3 dir = Vector3.zero;
                    Vector3 playerDir = Vector3.zero;

                    // Avoid player
                    if (_playerIsNear)
                    {
                        playerDir = _playerPosition - transform.position;
                        dir = -playerDir;
                        //Debug.LogFormat("<color=lime>{0}</color>", new Vector3(-playerDir.x, -playerDir.y, 0));
                        _playerIsNear = false;

                        if (GetTagAtPosition(transform.position + dir) == "Untagged")
                        {
                            Debug.LogFormat("<color=lime>{0}</color>", dir);
                            _nextDirection = dir;
                            _nextPositionMarker.transform.position = transform.position + dir;
                            break;
                        }
                    }
                    else
                    {
                        // Random position
                        dir = GetRandomDirection();
                        if (GetTagAtPosition(transform.position + dir) == "Untagged")
                        {
                            _nextDirection = dir;
                            _nextPositionMarker.transform.position = transform.position + dir;
                            break;
                        }
                    }
                }
                break;

            case Species.Fast:
                break;

            case Species.Big:
                break;

            case Species.Small:
                break;

            default:
                break;
        }

        // Line
        Vector3[] linePos = { transform.position, _nextPositionMarker.transform.position };
        _lineRenderer.positionCount = 2;
        _lineRenderer.SetPositions(linePos);
    }

    private Vector3 GetRandomDirection()
    {
        //Random.InitState(System.Environment.TickCount);
        float x = Mathf.RoundToInt(Random.Range(-1f, 1f));
        float y = Mathf.RoundToInt(Random.Range(-1f, 1f));
        return new Vector3(x, y, 0);
    }

    private void MoveToDirection(Vector3 direction)
    {
        transform.Translate(direction);
    }

    private void CheckNeighbors()
    {
        int i = 0;
        for (int y = -1; y <= 1; y++)
        {
            for (int x = -1; x <= 1; x++)
            {
                if (y == 0 && x == 0) continue;
                Vector3 pos = transform.position + new Vector3(x, y);
                string tag = GetTagAtPosition(pos);
                _neighbors[i] = new Tuple<Vector3, string>(pos, tag);

                if (tag == "Player")
                {
                    _playerIsNear = true;
                    _playerPosition = pos;
                }

                i++;
            }
        }
    }

    private string GetTagAtPosition(Vector3 pos)
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(pos, 0.1f);
        foreach (var collider in colliders)
        {
            if (collider.CompareTag("Rock"))
                return "Rock";

            if (collider.CompareTag("Player"))
                return "Player";

            if (collider.CompareTag("Animal"))
                return "Animal";
        }
        return "Untagged";
    }

    public void Die()
    {
        Destroy(gameObject);
    }
}