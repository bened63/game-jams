﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    [SerializeField] private PlayerModel _playerModel;
    [SerializeField] private TimeOfDay _timeOfDay;
    [SerializeField] private Transform _arrow;
    [SerializeField] private GameObject _goSiesta;
    [SerializeField] private GameObject _goFinish;
    private Camera _mainCam;
    private Rigidbody2D _rb;
    private AudioSource _audioSource;
    private float _horizontal;
    private float _vertical;
    private Animator _anim;
    private UIManager _ui;
    private int _currentLevel = 1;
    private Vector3 _targetDirection = Vector3.zero;
    private int _movementCosts = 1;
    private float _lastStaminaRecovery = 0f;
    private bool _doingSiesta = false;
    private bool _temperatureChanged = false;

    private void Awake()
    {
        _mainCam = Camera.main;
        _audioSource = GetComponent<AudioSource>();
        _anim = GetComponent<Animator>();
        _ui = FindObjectOfType<UIManager>();
        if (_ui == null) Debug.Log("<color=lime>No UIManager found.</color>");

        _playerModel.onTemperatureChanged += HandleOnTemperatureChanged;

        //_timeOfDay.onTimeChanged += (h, m) =>
        //{
        //    if (h == 12) _playerModel.temperature = PlayerModel.Temperature.Hot;
        //    if (h == 16) _playerModel.temperature = PlayerModel.Temperature.Normal;
        //};

        // Start stats
        _playerModel.health = 100;
        _playerModel.stamina = 20;
        _playerModel.food = 0;
        _playerModel.movePoints = 0;
        _playerModel.temperature = PlayerModel.Temperature.Normal;
    }

    private void HandleOnTemperatureChanged(PlayerModel.Temperature t)
    {
        _movementCosts = (t == PlayerModel.Temperature.Hot) ? 2 : 1;

        if (t == PlayerModel.Temperature.Normal) _goSiesta.SetActive(false);
        if (t == PlayerModel.Temperature.Hot) _goSiesta.SetActive(true);
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            ShowArrow();
        }
        if (Input.GetMouseButtonUp(0))
        {
            Move(_targetDirection);
            _arrow.gameObject.SetActive(false);
        }

        // Show Button Make Siesta for 2h!

        ///////////////////////////
        //_horizontal = Input.GetAxisRaw("Horizontal");
        //_vertical = Input.GetAxisRaw("Vertical");

        //if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D) ||
        //    Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S))
        //{
        //    Vector3 direction = new Vector3(_horizontal, _vertical, 0);
        //    Move(direction);
        //}
        ///////////////////////////

        if (_doingSiesta) DoSiesta();
    }

    private void OnDestroy()
    {
        _playerModel.onTemperatureChanged -= HandleOnTemperatureChanged;
    }

    private void Move(Vector3 direction)
    {
        // Move!
        if (!TriggerCheck(transform.position + direction, "Rock"))
        {
            transform.Translate(direction);
            ReduceStamina();
            StartCoroutine(IEAddMovePoints());
        }
    }

    private IEnumerator IEAddMovePoints(float delay = 0.2f)
    {
        yield return new WaitForSeconds(delay);
        _playerModel.movePoints++;
    }

    private void ReduceStamina()
    {
        _playerModel.stamina -= _movementCosts;

        if (!_temperatureChanged)
        {
            if (_playerModel.stamina <= 10)
            {
                _temperatureChanged = true;
                _playerModel.temperature = PlayerModel.Temperature.Hot;
            }
        }

        if (_playerModel.stamina <= 0)
        {
            if (!TriggerCheck(transform.position, "SiestaZone"))
                _ui.ShowGameOverMessage();
        }
    }

    private void ShowArrow()
    {
        _arrow.gameObject.SetActive(true);

        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hit.collider != null)
        {
            // Get position: One tile distance to player
            float x = Mathf.FloorToInt(hit.point.x + 0.5f);
            x = Mathf.Clamp(x, transform.position.x - 1f, transform.position.x + 1f);

            float y = Mathf.FloorToInt(hit.point.y + 0.5f);
            y = Mathf.Clamp(y, transform.position.y - 1f, transform.position.y + 1f);

            // Position where player wants to move
            Vector3 target = new Vector3(x, y, 0);

            // Direction vector
            Vector3 dir = target - transform.position;
            _targetDirection = new Vector3(Mathf.Clamp(dir.x, -1f, 1f), Mathf.Clamp(dir.y, -1f, 1f), 0);

            // Rotate arrow
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            _arrow.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Animal"))
        {
            Animal a = collision.GetComponent<Animal>();
            a.Die();
            _playerModel.food++;
        }

        if (collision.CompareTag("SiestaZone"))
        {
            _doingSiesta = true;
        }

        if (collision.CompareTag("Finish"))
        {
            SceneManager.LoadScene(2);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("SiestaZone"))
        {
            _doingSiesta = false;
        }
    }

    private bool TriggerCheck(Vector3 position, string tag)
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(position, 0.1f);
        foreach (var collider in colliders)
        {
            if (collider.CompareTag(tag))
                return true;
        }
        return false;
    }

    public void DoSiesta()
    {
        if (_playerModel.temperature == PlayerModel.Temperature.Normal)
            return;

        _goFinish.SetActive(true);
        _playerModel.stamina = 20;
        _playerModel.temperature = PlayerModel.Temperature.Normal;
        //_lastStaminaRecovery += Time.deltaTime;
        //if (_lastStaminaRecovery > 0.5f)
        //{
        //    if (_playerModel.stamina < 100) _playerModel.stamina++;
        //    _lastStaminaRecovery = 0f;
        //}
    }
}