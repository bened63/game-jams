﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "New Player Model", menuName = "Scriptable Objects/PlayerModel")]
public class PlayerModel : ScriptableObject
{
    public enum Temperature
    {
        Normal, Hot
    }

    private float _health = 100;
    private float _stamina = 100;
    private int _food = 0;
    private int _movePoints = 0;
    private float _timeOfDay = 6;
    private Temperature _temperature = Temperature.Normal;

    public Action<float> onHealthChanged;
    public Action<float> onStaminaChanged;
    public Action<int> onPositionChanged;
    public Action<int> onFoodChanged;
    public Action<Temperature> onTemperatureChanged;

    public float stamina
    {
        get => _stamina;
        set
        {
            _stamina = Mathf.Clamp(value, 0, float.PositiveInfinity);
            onStaminaChanged?.Invoke(_stamina);
        }
    }

    public float health
    {
        get => _health;
        set
        {
            _health = Mathf.Clamp(value, 0, float.PositiveInfinity);
            onHealthChanged?.Invoke(_health);
        }
    }

    public int food
    {
        get => _food;
        set
        {
            _food = value;
            onFoodChanged?.Invoke(value);
        }
    }

    public int movePoints
    {
        get => _movePoints;
        set
        {
            _movePoints = value;
            onPositionChanged?.Invoke(value);
        }
    }

    public Temperature temperature
    {
        get => _temperature;
        set
        {
            Debug.LogFormat("<color=lime>{0} : {1}</color>", temperature,value);
            if (_temperature != value)
            {
                _temperature = value;
                onTemperatureChanged?.Invoke(value);
            }
        }
    }
}